<?php

// Podrías implementarlo de otra forma?
abstract class figura{
    // Explicación del uso de protected en esta implementación, diferencias entre private, protected y public
    abstract protected function pintar();
}