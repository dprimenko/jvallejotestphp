<?php

require_once('triangulo.class.php');
require_once('cuadrado.class.php');
require_once('rombo.class.php');

// Nombre de clases en minúscula
class principal{

    // Uso de public para una propiedad
    public $lista;

    // Podrías implementar esta ejecución de otra forma?
    public function main(){
        $this->lista = array(
            new triangulo(),
            new cuadrado(),
            new triangulo(),
            new cuadrado(),
            new rombo()
        );

        foreach ($this->lista as $class) {
            if(is_subclass_of($class, 'paralelogramo')){
                $class->pintar();
                echo '<br/>';
            }
        }

        foreach ($this->lista as $class) {
            if(get_class($class) == 'triangulo'){
                $class->pintar();
                echo '<br/>';
            }
        }
    }
}